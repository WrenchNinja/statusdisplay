$(document).ready(function(){


	Firebase.INTERNAL.forceWebSockets();
	var beaconStatusRef = new Firebase('https://popping-inferno-9926.firebaseio.com/beacons/');
	var updateUI;
	var users;

	beaconStatusRef.on('value', function(dataSnapshot) {
		updateUI(dataSnapshot.val().status, dataSnapshot.val().name, dataSnapshot.val().color);
	});

	var setUser = function(){

		users = {
			WrenchNinja: {
				username : "WrenchNinja",
				name : "Kali Wever",
				points : 1000
			}, 
			CurlyChick: {
				username : "CurlyChick",
				name : "Demi Timmermans",
				points : 11300
			}, 
			HoutWurm: {
				username : "HoutWurm", 
				name : "Johan Boutkan",
				points : 34000
			}
		}

		var currentUser = {};

		var updateUser = function(user){
			$(".naam").html(user.name);
			$(".punten").html(user.points);
			console.dir(user);
		}

		$(document).on("keypress", function(e){

			if(e.keyCode == 49){
				$(".views").removeClass('member').removeClass('vip').addClass('non-member');
				currentUser = users.WrenchNinja;
			} else if(e.keyCode == 50){
				$(".views").removeClass('non-member').removeClass('vip').addClass('member');
				currentUser = users.CurlyChick;
			} else if(e.keyCode == 51){
				$(".views").removeClass('non-member').removeClass('member').addClass('vip');
				currentUser = users.HoutWurm;
			}

			updateUser(currentUser);

		});

	}


	/* init */

	setUser();

		var scale,
		depth_pass0,
		depth_pass1,
		depth_pass2,
		depth_pass3,
		depth_pass4,
		depth_pass5,
		depth_pass6,
		depth_pass7,
		depth_pass8,
		depth_pass9,
		depth_passAvg,
		range_pass0,
		range_pass1,
		range_pass2,
		range_pass3,
		range_pass4,
		range_pass5,
		range_pass6,
		range_pass7,
		range_pass8,
		range_pass9,
		range_passAvg;

	setInterval(function(){ 

		scale = analogPins[0];

		if(scale < 0){
			scale = 0;
		}

		depth_pass9 = depth_pass8;
		depth_pass8 = depth_pass7;
		depth_pass7 = depth_pass6;
		depth_pass6 = depth_pass5;
		depth_pass5 = depth_pass4;
		depth_pass4 = depth_pass3;
		depth_pass3 = depth_pass2;
		depth_pass2 = depth_pass1;
		depth_pass1 = depth_pass0;
		depth_pass0 = (scale * 0.015) * 2;
		depth_passAvg = (depth_pass0 + depth_pass1 + depth_pass2 + depth_pass3 + depth_pass4 + depth_pass5 + depth_pass6 + depth_pass7 + depth_pass8 + depth_pass9)/10;


		range_pass9 = range_pass8;
		range_pass8 = range_pass7;
		range_pass7 = range_pass6;
		range_pass6 = range_pass5;
		range_pass5 = range_pass4;
		range_pass4 = range_pass3;
		range_pass3 = range_pass2;
		range_pass2 = range_pass1;
		range_pass1 = range_pass0;
		range_pass0 = scale/10000;
		range_passAvg = (range_pass0 + range_pass1 + range_pass2 + range_pass3 + range_pass4 + range_pass5 + range_pass6 + range_pass7 + range_pass8 + range_pass9)/10;

		MESH.depth = depth_passAvg;
		MESH.xRange = range_passAvg;
		MESH.yRange = range_passAvg;

		$(".views .demo").css({
			
			//"width": analogPins[0]+"%",
			//"height": analogPins[1]+"%",
			//"transform": "translateX(-"+analogPins[0]+"px) translateY("+analogPins[1]+"px)"
			//"transform": "scale("+ (range_passAvg*10) +")"

		});

	},20);



	$(document).on("keypress", function(e){

		if(e.keyCode == 49){
			MESH.speed = 0.001;
			MESH.ambient = '#665caf';
			MESH.diffuse = '#ffa400';
			LIGHT.ambient = '#643f3f;'
			LIGHT.diffuse = '#423152';
			createMesh();
			createLights();
		} else if(e.keyCode == 50){
			MESH.speed = 0.001;
			MESH.ambient = '#5caf79';
			MESH.diffuse = '#efff00';
			LIGHT.ambient = '#3f4364';
			LIGHT.diffuse = '#826713';
			createMesh();
			createLights();
		} else if(e.keyCode == 51){
			MESH.speed = 0.001;
			MESH.ambient = '#ff7800';
			MESH.diffuse = '#d26200';
			LIGHT.ambient = '#000000';
			LIGHT.diffuse = '#2d1f07';
			createMesh();
			createLights();
		}

	});





	var updateUI = function(status, naam, color){

		if(status == 1){

			$(".views .demo").addClass('active');
			$(".naam").html(naam);

			MESH.speed = 0.001;

			if(color == 'purple'){
				MESH.ambient = '#665caf';
				MESH.diffuse = '#ffa400';
				LIGHT.ambient = '#643f3f;'
				LIGHT.diffuse = '#423152';
			} else if(color == 'green'){
				MESH.ambient = '#114da2';
				MESH.diffuse = '#ff6800';
				LIGHT.ambient = '#87861a;'
				LIGHT.diffuse = '#809813';
			} else if(color == 'red'){
				MESH.ambient = '#5f5f5f';
				MESH.diffuse = '#8e8e8e';
				LIGHT.ambient = '#871b1b;'
				LIGHT.diffuse = '#981313';
			} else if(color == 'orange'){
				MESH.ambient = '#a23311';
				MESH.diffuse = '#ff6800';
				LIGHT.ambient = '#87861a;'
				LIGHT.diffuse = '#809813';
			} else if(color == 'yellow'){
				MESH.ambient = '#99a211';
				MESH.diffuse = '#c0ba5c';
				LIGHT.ambient = '#b15b49;'
				LIGHT.diffuse = '#318282';
			} else if(color == 'blue'){
				MESH.ambient = '#1177a2';
				MESH.diffuse = '#c0ba5c';
				LIGHT.ambient = '#b15b49;'
				LIGHT.diffuse = '#419393';
			}
			
			createMesh();
			createLights();

		} else if( status == 0){

			$(".views .demo").removeClass('active');
			currentUser = users.WrenchNinja;

			MESH.speed = 0.001;
			MESH.ambient = '#b9b9b9';
			MESH.diffuse = '#575757';
			LIGHT.ambient = '#000000';
			LIGHT.diffuse = '#919191';
			createMesh();
			createLights();

		}
		
	}
	





	  //------------------------------
	  // Mesh Properties
	  //------------------------------
	  var MESH = {
	    width: 1.2,
	    height: 1.2,
	    depth: 11,
	    segments: 12,
	    slices: 8,
	    xRange: 0.05,
	    yRange: 0.05,
	    zRange: 1.0,
	    ambient: '#b9b9b9',
	    diffuse: '#575757',
	    speed: 0.001
	  };

	  //------------------------------
	  // Light Properties
	  //------------------------------
	  var LIGHT = {
	    count: 1.3,
	    xyScalar: 1,
	    zOffset: 100,
	    ambient: '#000000',
	    diffuse: '#919191',
	    speed: 0.001,
	    gravity: 1200,
	    dampening: 0.95,
	    minLimit: 10,
	    maxLimit: null,
	    minDistance: 20,
	    maxDistance: 400,
	    autopilot: true,
	    draw: false,
	    bounds: FSS.Vector3.create(),
	    step: FSS.Vector3.create(
	      Math.randomInRange(0.2, 1.0),
	      Math.randomInRange(0.2, 1.0),
	      Math.randomInRange(0.2, 1.0)
	    )
	  };

	  //------------------------------
	  // Render Properties
	  //------------------------------
	  var WEBGL = 'webgl';
	  var CANVAS = 'canvas';
	  var SVG = 'svg';
	  var RENDER = {
	    renderer: CANVAS
	  };

	  //------------------------------
	  // Export Properties
	  //------------------------------
	  var EXPORT = {
	    width: 2000,
	    height: 1000,
	    drawLights: false,
	    minLightX: 0.4,
	    maxLightX: 0.6,
	    minLightY: 0.2,
	    maxLightY: 0.4,
	    export: function() {
	      var l, x, y, light,
	          depth = MESH.depth,
	          zOffset = LIGHT.zOffset,
	          autopilot = LIGHT.autopilot,
	          scalar = this.width / renderer.width;

	      LIGHT.autopilot = true;
	      LIGHT.draw = this.drawLights;
	      LIGHT.zOffset *= scalar;
	      MESH.depth *= scalar;

	      resize(this.width, this.height);

	      for (l = scene.lights.length - 1; l >= 0; l--) {
	        light = scene.lights[l];
	        x = Math.randomInRange(this.width*this.minLightX, this.width*this.maxLightX);
	        y = Math.randomInRange(this.height*this.minLightY, this.height*this.maxLightY);
	        FSS.Vector3.set(light.position, x, this.height-y, this.lightZ);
	        FSS.Vector3.subtract(light.position, center);
	      }

	      update();
	      render();

	      switch(RENDER.renderer) {
	        case WEBGL:
	          window.open(webglRenderer.element.toDataURL(), '_blank');
	          break;
	        case CANVAS:
	          window.open(canvasRenderer.element.toDataURL(), '_blank');
	          break;
	        case SVG:
	          var data = encodeURIComponent(output.innerHTML);
	          var url = "data:image/svg+xml," + data;
	          window.open(url, '_blank');
	          break;
	      }

	      LIGHT.draw = true;
	      LIGHT.autopilot = autopilot;
	      LIGHT.zOffset = zOffset;
	      MESH.depth = depth;

	      resize(container.offsetWidth, container.offsetHeight);
	    }
	  };

	  //------------------------------
	  // UI Properties
	  //------------------------------
	  var UI = {
	    show: true
	  };

	  //------------------------------
	  // Global Properties
	  //------------------------------
	  var now, start = Date.now();
	  var center = FSS.Vector3.create();
	  var attractor = FSS.Vector3.create();
	  var container = document.getElementById('container');
	  var controls = document.getElementById('controls');
	  var output = document.getElementById('output');
	  var ui = document.getElementById('ui');
	  var renderer, scene, mesh, geometry, material;
	  var webglRenderer, canvasRenderer, svgRenderer;
	  var gui, autopilotController;

	  //------------------------------
	  // Methods
	  //------------------------------
	  function initialise() {
	    createRenderer();
	    createScene();
	    createMesh();
	    createLights();
	    addEventListeners();
	    addControls();
	    resize(container.offsetWidth, container.offsetHeight);
	    animate();
	  }

	  function createRenderer() {
	    webglRenderer = new FSS.WebGLRenderer();
	    canvasRenderer = new FSS.CanvasRenderer();
	    svgRenderer = new FSS.SVGRenderer();
	    setRenderer(RENDER.renderer);
	  }

	  function setRenderer(index) {
	    if (renderer) {
	      output.removeChild(renderer.element);
	    }
	    switch(index) {
	      case WEBGL:
	        renderer = webglRenderer;
	        break;
	      case CANVAS:
	        renderer = canvasRenderer;
	        break;
	      case SVG:
	        renderer = svgRenderer;
	        break;
	    }
	    renderer.setSize(container.offsetWidth, container.offsetHeight);
	    output.appendChild(renderer.element);
	  }

	  function createScene() {
	    scene = new FSS.Scene();
	  }

	  function createMesh() {
	    scene.remove(mesh);
	    renderer.clear();
	    geometry = new FSS.Plane(MESH.width * renderer.width, MESH.height * renderer.height, MESH.segments, MESH.slices);
	    material = new FSS.Material(MESH.ambient, MESH.diffuse);
	    mesh = new FSS.Mesh(geometry, material);
	    scene.add(mesh);

	    // Augment vertices for animation
	    var v, vertex;
	    for (v = geometry.vertices.length - 1; v >= 0; v--) {
	      vertex = geometry.vertices[v];
	      vertex.anchor = FSS.Vector3.clone(vertex.position);
	      vertex.step = FSS.Vector3.create(
	        Math.randomInRange(0.2, 1.0),
	        Math.randomInRange(0.2, 1.0),
	        Math.randomInRange(0.2, 1.0)
	      );
	      vertex.time = Math.randomInRange(0, Math.PIM2);
	    }
	  }

	  function createLights() {
	    var l, light;
	    for (l = scene.lights.length - 1; l >= 0; l--) {
	      light = scene.lights[l];
	      scene.remove(light);
	    }
	    renderer.clear();
	    for (l = 0; l < LIGHT.count; l++) {
	      light = new FSS.Light(LIGHT.ambient, LIGHT.diffuse);
	      light.ambientHex = light.ambient.format();
	      light.diffuseHex = light.diffuse.format();
	      scene.add(light);

	      // Augment light for animation
	      light.mass = Math.randomInRange(0.5, 1);
	      light.velocity = FSS.Vector3.create();
	      light.acceleration = FSS.Vector3.create();
	      light.force = FSS.Vector3.create();

	      // Ring SVG Circle
	      light.ring = document.createElementNS(FSS.SVGNS, 'circle');
	      light.ring.setAttributeNS(null, 'stroke', light.ambientHex);
	      light.ring.setAttributeNS(null, 'stroke-width', '0.5');
	      light.ring.setAttributeNS(null, 'fill', 'none');
	      light.ring.setAttributeNS(null, 'r', '10');

	      // Core SVG Circle
	      light.core = document.createElementNS(FSS.SVGNS, 'circle');
	      light.core.setAttributeNS(null, 'fill', light.diffuseHex);
	      light.core.setAttributeNS(null, 'r', '4');
	    }
	  }

	  function resize(width, height) {
	    renderer.setSize(width, height);
	    FSS.Vector3.set(center, renderer.halfWidth, renderer.halfHeight);
	    createMesh();
	  }

	  function animate() {
	    now = Date.now() - start;
	    update();
	    render();
	    requestAnimationFrame(animate);
	  }

	  function update() {
	    var ox, oy, oz, l, light, v, vertex, offset = MESH.depth/2;

	    // Update Bounds
	    FSS.Vector3.copy(LIGHT.bounds, center);
	    FSS.Vector3.multiplyScalar(LIGHT.bounds, LIGHT.xyScalar);

	    // Update Attractor
	    FSS.Vector3.setZ(attractor, LIGHT.zOffset);

	    // Overwrite the Attractor position
	    if (LIGHT.autopilot) {
	      ox = Math.sin(LIGHT.step[0] * now * LIGHT.speed);
	      oy = Math.cos(LIGHT.step[1] * now * LIGHT.speed);
	      FSS.Vector3.set(attractor,
	        LIGHT.bounds[0]*ox,
	        LIGHT.bounds[1]*oy,
	        LIGHT.zOffset);
	    }

	    // Animate Lights
	    for (l = scene.lights.length - 1; l >= 0; l--) {
	      light = scene.lights[l];

	      // Reset the z position of the light
	      FSS.Vector3.setZ(light.position, LIGHT.zOffset);

	      // Calculate the force Luke!
	      var D = Math.clamp(FSS.Vector3.distanceSquared(light.position, attractor), LIGHT.minDistance, LIGHT.maxDistance);
	      var F = LIGHT.gravity * light.mass / D;
	      FSS.Vector3.subtractVectors(light.force, attractor, light.position);
	      FSS.Vector3.normalise(light.force);
	      FSS.Vector3.multiplyScalar(light.force, F);

	      // Update the light position
	      FSS.Vector3.set(light.acceleration);
	      FSS.Vector3.add(light.acceleration, light.force);
	      FSS.Vector3.add(light.velocity, light.acceleration);
	      FSS.Vector3.multiplyScalar(light.velocity, LIGHT.dampening);
	      FSS.Vector3.limit(light.velocity, LIGHT.minLimit, LIGHT.maxLimit);
	      FSS.Vector3.add(light.position, light.velocity);
	    }

	    // Animate Vertices
	    for (v = geometry.vertices.length - 1; v >= 0; v--) {
	      vertex = geometry.vertices[v];
	      ox = Math.sin(vertex.time + vertex.step[0] * now * MESH.speed);
	      oy = Math.cos(vertex.time + vertex.step[1] * now * MESH.speed);
	      oz = Math.sin(vertex.time + vertex.step[2] * now * MESH.speed);
	      FSS.Vector3.set(vertex.position,
	        MESH.xRange*geometry.segmentWidth*ox,
	        MESH.yRange*geometry.sliceHeight*oy,
	        MESH.zRange*offset*oz - offset);
	      FSS.Vector3.add(vertex.position, vertex.anchor);
	    }

	    // Set the Geometry to dirty
	    geometry.dirty = true;
	  }

	  function render() {
	    renderer.render(scene);

	    // Draw Lights
	    if (LIGHT.draw) {
	      var l, lx, ly, light;
	      for (l = scene.lights.length - 1; l >= 0; l--) {
	        light = scene.lights[l];
	        lx = light.position[0];
	        ly = light.position[1];
	        switch(RENDER.renderer) {
	          case CANVAS:
	            renderer.context.lineWidth = 0.5;
	            renderer.context.beginPath();
	            renderer.context.arc(lx, ly, 10, 0, Math.PIM2);
	            renderer.context.strokeStyle = light.ambientHex;
	            renderer.context.stroke();
	            renderer.context.beginPath();
	            renderer.context.arc(lx, ly, 4, 0, Math.PIM2);
	            renderer.context.fillStyle = light.diffuseHex;
	            renderer.context.fill();
	            break;
	          case SVG:
	            lx += renderer.halfWidth;
	            ly = renderer.halfHeight - ly;
	            light.core.setAttributeNS(null, 'fill', light.diffuseHex);
	            light.core.setAttributeNS(null, 'cx', lx);
	            light.core.setAttributeNS(null, 'cy', ly);
	            renderer.element.appendChild(light.core);
	            light.ring.setAttributeNS(null, 'stroke', light.ambientHex);
	            light.ring.setAttributeNS(null, 'cx', lx);
	            light.ring.setAttributeNS(null, 'cy', ly);
	            renderer.element.appendChild(light.ring);
	            break;
	        }
	      }
	    }
	  }

	  function addEventListeners() {
	    window.addEventListener('resize', onWindowResize);
	    container.addEventListener('click', onMouseClick);
	    container.addEventListener('mousemove', onMouseMove);
	  }

	  function addControls() {
	   
	  }

	  //------------------------------
	  // Callbacks
	  //------------------------------
	  function onMouseClick(event) {
	    FSS.Vector3.set(attractor, event.x, renderer.height - event.y);
	    FSS.Vector3.subtract(attractor, center);
	    LIGHT.autopilot = !LIGHT.autopilot;
	    autopilotController.updateDisplay();
	  }

	  function onMouseMove(event) {
	    FSS.Vector3.set(attractor, event.x, renderer.height - event.y);
	    FSS.Vector3.subtract(attractor, center);
	  }

	  function onWindowResize(event) {
	    resize(container.offsetWidth, container.offsetHeight);
	    render();
	  }



	  // Let there be light!
	  initialise();

	



});